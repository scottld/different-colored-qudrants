import pygame as pg
pg.init()

screen = pg.display.set_mode((500,500))

def colorQuadrants(surface, mouse_pos):
    mouse_x = mouse_pos[0]
    mouse_y = mouse_pos[1]
    screen_width = surface.get_width()
    screen_height = surface.get_height()

    #top left rectangle
    pg.draw.rect(surface, (255,0,0), (0, 0, mouse_x, mouse_y))
    #top right rectangle
    pg.draw.rect(surface, (0, 255, 0), (mouse_x, 0, screen_width - mouse_x, mouse_y))
    #bottom left rectangle
    pg.draw.rect(surface, (0, 0, 255), (0, mouse_y, mouse_x, screen_height - mouse_y))
    #bottom right rectangle
    pg.draw.rect(surface, (255, 255, 0), (mouse_x, mouse_y, screen_width - mouse_x, screen_height - mouse_y))

    '''
    #draw vertical line
    startPosVerticalLine = (mouse_position[0], 0)
    endPosVerticalLine = (mouse_position[0], screen.get_height())
    pg.draw.line(screen, (255,255,255), startPosVerticalLine, endPosVerticalLine)

    #draw vertical line
    startPosHorisontalLine = (0,mouse_position[1])
    endPosHorisontalLine = (screen.get_width(),mouse_position[1])
    pg.draw.line(screen, (255,255,255), startPosHorisontalLine, endPosHorisontalLine)
    '''


running = True
while running:
    for event in pg.event.get():
        if event.type == pg.QUIT:
            running = False
    

    screen.fill((0,0,0))
    
    mouse_position = pg.mouse.get_pos()
    colorQuadrants(screen, mouse_position)
    pg.display.flip()